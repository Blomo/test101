import logo from './logo.svg';
import './App.css';
import  Header from './Components/Header';
import Intro from './Components/Intro-Page';
import About from './Components/About-page';
import WhatsHot from './Components/What\'sHot-page';
import ExpansionPage from './Components/Expansion-page';
import Listen from './Components/Listen';
import ChooseYourPlan from './Components/ChooseYourPlan/index';

function App() {
  return (
    <div className="App">
      <Header/>
      <Intro/>
      <About/>
      <WhatsHot/>
      <ExpansionPage/>
      <Listen/>
      <ChooseYourPlan/>
      
    </div>
  );
}

export default App;
