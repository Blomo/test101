import React from "react"
import { Container } from "./styles"
import PhoneImage from "../../Assets/20.png"

const About = () => {
    return (
        <Container>
            <div className="imageConatiner">

                <img src={PhoneImage} height="600" />
            </div>
            <div className="aboutTextContainer">


                <h1 className="Header">
                    About Napollo
                </h1>
            <div className="textContainer">
                <p>
                    Napollo is a music streaming service that connects artistes to listeners in a way that has never been done before.
                    
                </p>
                
            </div>

            <div className="textContainer">
                <p>
                    Artists,musicians,music lovers, producers, no matter your taste or skills are, we are
 here to serve you the best all around the world                </p>
                
            </div>
            </div>

        </Container>
    )
}
export default About;