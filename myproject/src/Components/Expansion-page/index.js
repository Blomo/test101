import React from "react"
import { Container } from "./styles"
import PhoneImage from "../../Assets/19.png"
import First from "../../Assets/18.png"
import second from "../../Assets/14.png"
import third from "../../Assets/6.png"
import Logo from "../../Assets/sound.png"

import {FcLike} from "react-icons/fc"

import { IoIosArrowForward } from "react-icons/io"

const ExpansionPage = () => {
 
    const cards = [
        {
            number:"01",
            image: First,
            name: "Lights Out",
            author: "ColdPlay",
        },
        {
            number:"02",
            image: second,
            name: "   ",
            author: "Green Tuska",
        },
        {
            number:"03",
            image: third,
            name: "    ",
            author: "Bruno Mars",
        },
        {
            number:"04",
            image: First,
            name: "  ",
            author: "Cardi",
        },

    ]



    return (
        <Container>
            <div className="imageConatiner">

                <img src={PhoneImage} height="400" />
                
            </div>
            
            <div className="section1Container">


                <h1 className="Header">
                    Expansion Page
                </h1>

                <div className="textContainer">
                <p>
                    Discover artistes all around the world by their location via the expansion page.Also,get to know the top artistes in any location on the globe.   
                </p>
                
            </div>
     

                <div className="playContainer">
                    <div className="albumSection" >

                        {cards.map(({ number,image, name, author }) => (
                            <div className="longCard">
                                  <p className="number">{number}</p>
                              <div className="imageCard">
                              <img src={image} className="image"/>
                              </div>
                              <div className="className">
                                  <p className="name">{name}</p>
                                  <p className="author">{author}</p>

                              </div>
                              <div>

                                  <FcLike size="24" color="blue"/>
                              </div>

                            </div>

                        ))}
                    </div>

                    </div>




                </div>


            

        </Container>
    )
}
export default ExpansionPage;