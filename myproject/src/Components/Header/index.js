import React from "react"
import { Container } from "./styles"
import { GiHamburgerMenu } from "react-icons/gi"
import { BiSearch } from "react-icons/bi"
import Logo from "../../Assets/20.png"
import GogglePlayStore from "../../Assets/3.png"
import Appstore from "../../Assets/4.png"
import {BsUpload}  from "react-icons/bs"

const Header = () => {
    return (
        <Container >
            <div className="iconmenu">
                <GiHamburgerMenu size="30" color="white" />
            </div>
            <div>
                <img src={Logo} alt="kmdm" height="90px" />
            </div>
            <div className="inputContainer">
                <div className="searchBar">
                    <input className="input" />
                    <BiSearch size="30" />
                </div>
            </div>
            <div>
                    <img src={GogglePlayStore} height="60"/>
                  
                </div>
                <div>
                    
                    <img src={Appstore} height="60"/>
                </div>
                <div className="Login_SignUp">
                    <p className="signUp">SignUp/</p>
                    <p className="signUp">SignIn</p>
                </div>
                

                <div>
                    <button className="button"> 
                       
                        <p className="Upload">  <BsUpload size="20"/> Upload </p>
                    </button>
                </div>




        </Container>
    )
}
export default Header