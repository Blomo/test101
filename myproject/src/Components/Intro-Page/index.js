import React from "react"
import { Container } from "./styles"
import IntroImage from "../../Assets/7.png"

const Intro = () => {
    return (
        <Container>
            <div className="imageConatiner">

                <img src={IntroImage} height="600" />
            </div>
            <div className="textContainer">
                <p>
                    More fans,More Gigs,Less Effort Get better feedblack,wider exposure and deeper industry access without ever switching tab.
                </p>
            </div>

        </Container>
    )
}
export default Intro;