import React from "react"
import Tape from "./../../Assets/2.png"
import { Container } from "./style"
const Listen = () => {

    const textArray=[
        {
            word:"What's hot"
        },
        {
            word:"By genre"
        },
        {
            word:"By location"
        },
        {
            word:"Lot's more..."
        }
    ]
    return (
        <Container>
            <div className="imageContainer">
                <img src={Tape} />
            </div>

            

            <div className="textContainer">
                <h1 className="Header">
                    Listen and enjoy all trending music
                </h1>

               
            {textArray.map(({word})=>(
                // <div>
                    <div className="List">
                        <div className="dot"></div>
                        <p>
                            {word}
                        </p>

                    </div>
                // </div>
                
            ))}
            <div className="buttonContainer">

            <button>Start listening</button>
            </div>
            </div>


        </Container>
    )
}
export default Listen;
