import styled from 'styled-components';


export const Container = styled.div`
 background-color:#f68126;
 height:500px;
 /* justify-content: center; */
     display: flex;
     align-items:center;
     flex-direction:row;
     

     .imageContainer{
         width:40%;
         align-items: center;
         display: flex;
         justify-content: center; 
         img{
             height: 300px;
             width: 400px;
             display: flex;
             resize: contain;
         }
     }

     .textContainer{
        width:40%;
        height: 500px;
        
        justify-content: flex-start;
         .Header{
        
        
        font-weight:bold;
        color:black;
        text-align:start;
        display: flex;
        align-content: flex-start;
        font-size:70px;
        width:85%;
       
    }
    .List{
        width: 300px;
        height: 40px;
        display: flex;
        align-items:center;
        /* word-spacing:5px */

    }
     }
    .dot{
        display:flex;
        border-radius:50px;
        height:10px ;
        width: 10px;
        margin-right:5px;
        background-color:yellow;
    }
    .buttonContainer{
        display: flex;
        justify-content: flex-start;
        /* width:100px */
        
        button{
            border: 0px;
            width:170px;
            height:35px;
            background-color:black;
            border-radius:30px;
            color:#f68126
    
        }
    }
 
`

