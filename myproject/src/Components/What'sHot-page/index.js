import React from "react"
import { Container } from "./styles"
import PhoneImage from "../../Assets/19.png"
import First from "../../Assets/18.png"
import second from "../../Assets/14.png"
import third from "../../Assets/6.png"
import Logo from "../../Assets/sound.png"
import Song1 from "../../Assets/17.png"
import Song2 from "../../Assets/16.png"
import Song3 from "../../Assets/15.png"
import { IoIosArrowForward } from "react-icons/io"

const WhatsHot = () => {
    const card = [
        {
            image: Song1,
            name: "Let's Dance",
            author: "Bowie",
        },
        {
            image: Song2,
            name: "Bad Romance",
            author: "Rihanna",
        },
        {
            image: Song3,
            name: "Fly",
            author: "Coldplay",
        },

    ]
    const cards = [
        {
            image: First,
            name: "Death Bed",
            author: "Powerful",
        },
        {
            image: second,
            name: "Faded",
            author: "Alan Walker",
        },
        {
            image: third,
            name: "Hate Me",
            author: " Ellie Goulding",
        },

    ]



    return (
        <Container>
            <div className="imageConatiner">

                <img src={PhoneImage} height="400" />
                
            </div>
            <div className="moveContainer">
                <div className="move">

            <IoIosArrowForward size="50"  color="white"/>

                </div>
            </div>
            <div className="section1Container">


                <h1 className="Header">
                    What's Hot!
                </h1>
                <div className="althurBar">


                    <label className="album">Albums</label>
                    <label className="viewAll">View-All <IoIosArrowForward size="15" /></label>
                </div>

                <div className="playContainer">
                    <div className="albumSection" >

                        {cards.map(({ image, name, author }) => (
                            <div className="cardContainer">
                                <div className="card">
                                    <img src={image} />
                                    <img src={Logo} className="logo" />

                                </div>
                                <div className="abultDetails">
                                    <p className="name">{name}</p>
                                    <p className="author">{author}</p>
                                </div>

                            </div>

                        ))}
                    </div>

                    <div className="althurBar">


                        <label className="album">Singles & EPs</label>
                        <label className="viewAll">View-All <IoIosArrowForward size="15" /></label>
                    </div>

                    <div className="albumSection" >



                        {card.map(({ image, name, author }) => (
                            <div className="cardContainer">
                                <div className="card">
                                    <img src={image} />
                                    <img src={Logo} className="logo" />

                                </div>
                                <div className="abultDetails">
                                    <p className="name">{name}</p>
                                    <p className="author">{author}</p>
                                </div>

                            </div>

                        ))}
                    </div>




                </div>


            </div>

        </Container>
    )
}
export default WhatsHot;